#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup  # pyre-ignore
import do_space_objects_interface as my_package

setup(
    name='do_space_objects_interface',
    version=my_package.__version__,
    author='dh4rm4',
    description='Provides an interface for DigitalOcean space objects',
    install_requires=[],
    classifiers=[
        "Programming Language :: Python 3.7.2",
        "Development Status :: Never ending",
        "Language :: English",
        "Operating System :: Debian based",
    ],
)
