#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    This module provides an interface for DigitalOcean
    space objects
"""


__version__ = '1.0'

from do_space_objects_interface.interface import \
    space_objects_interface
