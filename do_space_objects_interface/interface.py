import boto3
import botocore
from os import getenv

from custom_py_exceptions import \
    DoSpace_BucketDoesNotExist, \
    DoSpace_FileToUploadDoesNotExist, \
    DoSpace_FilenameToUploadTypeError, \
    DoSpace_InvalidUploadedFilename, \
    DoSpace_InvalidBucketVarType, \
    DoSpace_InvalidExpirationTime, \
    DoSpace_InvalidExpirationType


class space_objects_interface:
    """
        Manage files in an digitalocean space:
            - upload
            - download
    """
    def __init__(self, bucket_name):
        """
            bucket_name: name of the DO space to play with
        """
        self.bucket_name = bucket_name
        self.SPACE_REGION_TAG = self._get_space_region_tag()
        self.SPACE_ACCESS_ID = getenv('DO_VAR_SPACES_ACCESS_ID',
                                      'ERROR_MISSING_VAR')
        self.SPACE_ACCESS_KEY = getenv('DO_VAR_SPACES_ACCESS_KEY',
                                       'ERROR_MISSING_VAR')
        self.do_space_client = self.init_space_connection()

    def _get_space_region_tag(self):
        tag = getenv('DO_VAR_SPACES_REGION_TAG',
                     'ERROR_MISSING_VAR')
        if tag not in ['fra1', 'nyc3', 'sfo2', 'sgp1']:
            err_msg = 'Invalid DigitalOcean Region tag: ' + str(tag)
            raise ValueError(err_msg)
        return tag

    def init_space_connection(self):
        session = boto3.session.Session()
        endpoint_space_url = 'https://' + \
                             str(self.SPACE_REGION_TAG) + \
                             '.digitaloceanspaces.com'
        client = session.client('s3', region_name=self.SPACE_REGION_TAG,
                                endpoint_url=endpoint_space_url,
                                aws_access_key_id=self.SPACE_ACCESS_ID,
                                aws_secret_access_key=self.SPACE_ACCESS_KEY)
        return client

    def upload_file(self,
                    local_file_path: str,
                    dest_filename: str):
        """
            Upload file in a DigitalOcean Spaces.
                param1: (str) file to upload path
                param2: (str) destination filename
        """
        try:
            self._send_file_content(local_file_path,
                                    dest_filename)
        except FileNotFoundError:
            raise DoSpace_FileToUploadDoesNotExist(local_file_path)
        except TypeError:
            raise DoSpace_InvalidBucketVarType(type(self.bucket_name))
        except ValueError as err:
            if 'Filename' in err.args[0]:
                raise DoSpace_FilenameToUploadTypeError(type(local_file_path))
            raise
        except boto3.exceptions.S3UploadFailedError as err:
            if 'NoSuchBucket' in err.args[0]:
                raise DoSpace_BucketDoesNotExist(self.bucket_name) from err
            raise
        except botocore.exceptions.ParamValidationError as err:
            if 'for parameter Key' in err.args[0]:
                raise DoSpace_InvalidUploadedFilename(dest_filename) from err
            raise

    def _send_file_content(self,
                           local_file_path: str,
                           dest_filename: str):
        """
            Uses the goto3 session object to upload the given file
                - local_file_path: file to upload path
                - dest_filename: destination filename
        """
        self.do_space_client.upload_file(local_file_path,
                                         self.bucket_name,
                                         dest_filename)

    def download_file(self, *args, **kwargs):
        """
            Download file from a DigitalOcean Spaces.
                param1: (str) file to download
                param2: (str) destination path
        """
        try:
            self._download_file_content(*args, **kwargs)
        except botocore.exceptions.ClientError as err:
            if 'Not Found' in err.args[0]:
                print('Remote file does not exist.')
            else:
                raise Exception(err.args[0])
        except PermissionError:
            print('Permission error on destination file')

    def _download_file_content(self,
                               file_to_download: str,
                               dest_filepath: str):
        """
            Uses the goto3 session object to download asked file
                - file_to_download: name of the remote file
                - dest_filename: path file to store downloaded content
        """
        self.do_space_client.download_file(self.bucket_name,
                                           file_to_download,
                                           dest_filepath)

    def fetch_quick_link(self, filename: str, expiration: int):
        """
            Ask DO Api for a quick link to download the file
                - filename: (str) filename link
                - expiration: (int) time before link expired (in seconds)
        """
        if not isinstance(expiration, int):
            raise DoSpace_InvalidExpirationType(type(expiration))
        if (expiration < 60):
            raise DoSpace_InvalidExpirationTime(expiration)
        self._request_quick_link_to_do(filename, expiration)

    def _request_quick_link_to_do(self, filename: str, expiration: int):
        """
            Uses the goto3 session object to request a quick link
            (used to share with user)
                - filename: (str) space object filename
                - expiration: (str) time before link becomes invalid
        """
        self.quick_link = self.do_space_client.generate_presigned_url(ClientMethod='get_object',
                                                                      Params={'Bucket': self.bucket_name,
                                                                              'Key': filename},
                                                                      ExpiresIn=3600)
