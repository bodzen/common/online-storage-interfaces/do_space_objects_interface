#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
from parameterized import parameterized
from custom_py_exceptions import \
    DoSpace_BucketDoesNotExist, \
    DoSpace_FilenameToUploadTypeError, \
    DoSpace_InvalidUploadedFilename, \
    DoSpace_FileToUploadDoesNotExist, \
    DoSpace_InvalidBucketVarType
from do_space_objects_interface import space_objects_interface


class invalid_upload_paramters(unittest.TestCase):
    @parameterized.expand([
        ['invalid_space_name', 'INVALID', 'README.md', 'NONE',
         DoSpace_BucketDoesNotExist],
        ['invalid_space_name_type', None, 'README.md', 'NONE',
         DoSpace_InvalidBucketVarType],
        ['invalid_type_filename_to_upload', 'NONE', None, 'NONE',
         DoSpace_FilenameToUploadTypeError],
        ['invalid_destination_filename', 'NONE', 'README.md', None,
         DoSpace_InvalidUploadedFilename],
        ['file_to_upload_does_not_exist', 'NONE', 'does_not_exist.txt', 'NONE',
         DoSpace_FileToUploadDoesNotExist],
    ])
    def test_generic(self, name: str, space_name: str, local_file: str,
                     remote_filename: str, expected_exception: type):
        inst = space_objects_interface(space_name)
        self.assertRaises(expected_exception,
                          inst.upload_file,
                          local_file,
                          remote_filename)
