#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    Validate the datacenters region tag handling
    WARNING: This test modify environnement variables,
             It should be executed last in order (zzz)
"""

import unittest
from os import environ
from do_space_objects_interface import space_objects_interface
from parameterized import parameterized  # pyre-ignore


class region_tag_validity(unittest.TestCase):
    @parameterized.expand([
        ['alpha', 'fra2'],
        ['beta', 'sgp2'],
        ['gamma', 'sfo3'],
        ['delta', 'sfo1'],
        ['epsilon', 'nyc1']
    ])
    def test_invalid(self, name: str, region_tag: str):
        environ['DO_VAR_SPACES_REGION_TAG'] = region_tag
        self.assertRaises(ValueError, space_objects_interface, 'whatever')

    @parameterized.expand([
        ['alpha', 'fra1'],
        ['beta', 'nyc3'],
        ['gamma', 'sfo2'],
        ['delta', 'sgp1']
    ])
    def test_valid(self, name: str, region_tag: str):
        environ['DO_VAR_SPACES_REGION_TAG'] = region_tag
        self.assertEqual(space_objects_interface, type(space_objects_interface('whatever')))
