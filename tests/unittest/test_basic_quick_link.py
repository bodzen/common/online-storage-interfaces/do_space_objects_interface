#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os import getenv, remove
from os.path import getsize
import wget
import unittest
from parameterized import parameterized
from do_space_objects_interface import space_objects_interface


class basic_quick_link_generation(unittest.TestCase):
    def setUp(self):
        bucket_name = getenv('DO_SPACE_NAME', 'MISSING_VAR')
        self.inst = space_objects_interface(bucket_name)

    @parameterized.expand([
        ['alpha', '1024'],
        ['beta', '2048'],
        ['gamma', '4096'],
        ['delta', '8192']
    ])
    def test_generic(self, name, filesize):
        # Mock
        remote_file = self._create_remote_file(filesize)
        self.inst.fetch_quick_link(remote_file, 60)
        wget.download(url=self.inst.quick_link,
                      out=remote_file,
                      bar=None)
        # Test
        self.assertEqual(getsize(remote_file), int(filesize))
        remove(remote_file)

    def _create_remote_file(self, filesize: str):
        filename = 'basic_quick_link_test_' + filesize + '.remote'
        with open(filename, 'w') as f:
            f.truncate(int(filesize))
            self.inst.upload_file(filename, filename)
        remove(filename)
        return filename
