#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
from parameterized import parameterized
from do_space_objects_interface import space_objects_interface
from custom_py_exceptions import \
    DoSpace_InvalidExpirationType, \
    DoSpace_InvalidExpirationTime


class basic_quick_link_generation(unittest.TestCase):
    def setUp(self):
        self.inst = space_objects_interface('XXXX')

    @parameterized.expand([
        ['alpha', 1024],
        ['beta', 2048],
        ['gamma', 4096],
        ['delta', 8192]
    ])
    def test_valid_generic(self, name, expiration):
        self.inst.fetch_quick_link('XXXX', expiration)

    @parameterized.expand([
        ['alpha', None, DoSpace_InvalidExpirationType],
        ['beta', '420', DoSpace_InvalidExpirationType],
        ['gamma', 0, DoSpace_InvalidExpirationTime],
        ['delta', -420, DoSpace_InvalidExpirationTime],
        ['epsilon', 30, DoSpace_InvalidExpirationTime]
    ])
    def test_error_generic(self, name, expiration, expected_exception):
        self.assertRaises(expected_exception,
                          self.inst.fetch_quick_link, 'XXXX', expiration)
