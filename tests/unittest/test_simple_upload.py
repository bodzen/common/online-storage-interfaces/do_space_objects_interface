#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
from do_space_objects_interface import space_objects_interface
from os import getenv, remove
from os.path import getsize
from parameterized import parameterized  # pyre-ignore


class simple_upload(unittest.TestCase):
    def setUp(self):
        space_name = getenv('DO_SPACE_NAME', 'MISSING_VAR')
        self.inst = space_objects_interface(space_name)

    @parameterized.expand([
        ['alpha', '1024'],
        ['beta', '2048'],
        ['gamma', '4096'],
        ['delta', '8192']
    ])
    def test_generic(self, name: str, filesize: str):
        # Mock
        remote_file = self._create_remote_file(filesize)
        dest_filename = remote_file.replace('remote', 'local')
        self.inst.download_file(remote_file, dest_filename)
        # Test
        self.assertEqual(getsize(dest_filename), int(filesize))
        remove(remote_file)
        remove(dest_filename)

    def _create_remote_file(self, filesize: str):
        filename = 'simple_upload_test_' + filesize + '.remote'
        with open(filename, 'w') as f:
            f.truncate(int(filesize))
        self.inst.upload_file(filename, filename)
        return filename
