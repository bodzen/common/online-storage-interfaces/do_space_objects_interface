


# How to Test

## Unittest

```
export DO_VAR_SPACES_REGION_TAG=fra1
export DO_VAR_SPACES_ACCESS_ID=XXXX
export DO_VAR_SPACES_ACCESS_KEY=XXXX
export DO_SPACE_NAME=uniq_space_name

pip3 install -r requirements.txt
python -m unittest discover tests/unittest/
```
